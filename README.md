### This is a simple CRUD application
#### To run the file, clone this repo
- clone this repo 
- install the dependencies (pip3 install requirements.txt)
- create the sql database like so in a python3 environment:
  - `from bookmanager import db`
  - `db.create_all()`
#### Running the app
  - `python3 bookmanager.py`
  

