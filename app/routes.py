from app import app,db
from flask import render_template, flash, redirect, url_for, request
from app.models import Book, User
from flask_login import login_user, logout_user, current_user, login_required
from app.forms import LoginForm, RegistrationForm
from werkzeug.urls import url_parse


@app.route("/login", methods=["GET","POST"])
def login():
  if current_user.is_authenticated:
    return redirect(url_for('index'))
  form = LoginForm()
  if form.validate_on_submit():
    user = User.query.filter_by(username = form.username.data).first()
    if user is None or not user.check_password(form.password.data):
      flash('Invalid usernam or password')
      return redirect(url_for('login'))
    login_user(user, remember=form.remember_me.data)
    next_page = request.args.get('next')
    if not next_page or url_parse(next_page).netloc != '':
      next_page = url_for('index')
    return redirect(next_page)
  return render_template('login.html', title='Sign in', form=form)


@app.route("/logout")
def logout():
  logout_user()
  return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/', methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
@login_required
def index():
  if request.form:
    try:
      book = Book(title=request.form.get("title"), 
                  author=request.form.get("author"),
                  owner = current_user)
      db.session.add(book)
      db.session.commit()
    except Exception as e:
      print("Failed to add book")
      print(e)
  books = Book.query.filter_by(user_id=current_user.id)
  return render_template("index.html", books=books)


@app.route("/update", methods=["POST"])
def update():
  try:
    newtitle = request.form.get("newtitle")
    oldtitle = request.form.get("oldtitle")
    book = Book.query.filter_by(title=oldtitle).first()
    book.title = newtitle
    db.session.commit()
  except Exception as e:
    print("Couldn't update book title")
    print(e)
  return redirect("/")


@app.route("/delete",methods=["POST"])
def delete():
  title = request.form.get("title")
  book = Book.query.filter_by(title=title).first()
  db.session.delete(book)
  db.session.commit()
  return redirect("/")
